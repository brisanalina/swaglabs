import dev.failsafe.internal.util.Assert;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.function.Try;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.sql.Driver;
import java.time.Duration;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class ApplicationTesting {

    @Test
    void testWhenUserAndPasswordAreCorrect(){
        WebDriver driver= new ChromeDriver();
        driver.get("https://www.saucedemo.com/inventory.html"); //access the website
        driver.manage().window().maximize(); //maximize the window

        WebElement userNameTextBox=driver.findElement(By.xpath("//*[@id=\"user-name\"]"));     //identify username text box
        WebElement passwordTextBox=driver.findElement(By.xpath("//*[@id=\"password\"]"));      //identify password text box
        WebElement loginButton=driver.findElement(By.xpath("//*[@id=\"login-button\"]"));      //identify Login Button
        Actions a= new Actions(driver);        //create a new object (Actions type)

        //1.Enter "standard_user" TEXT in Username field
        a.moveToElement(userNameTextBox).click().sendKeys("standard_user");

        //2. BUILD and PERFORM
        a.build().perform();

        //3.Enter "secret_sauce" TEXT in Password field
        a.moveToElement(passwordTextBox).click().sendKeys("secret_sauce");

        //4. BUILD and PERFORM
        a.build().perform();

        //5. Click LOGIN Button
        a.click(loginButton);

        //6. BUILD and PERFORM
        a.build().perform();

        String result="";
        try {
            WebElement expected = driver.findElement(By.xpath("//*[@id=\"page_wrapper\"]/footer/ul/li[2]/a"));
            result="OK";

        }catch(Exception e){
            result="ERROR";
        }
        assertEquals("OK",result);
    }

    @Test
    void testWhenUserIsCorrectAndPasswordIsIncorrect(){
        WebDriver driver= new ChromeDriver();
        driver.get("https://www.saucedemo.com/inventory.html"); //access the website
        driver.manage().window().maximize();

        WebElement userNameTextBox=driver.findElement(By.xpath("//*[@id=\"user-name\"]"));
        WebElement passwordTextBox=driver.findElement(By.xpath("//*[@id=\"password\"]"));
        WebElement loginButton=driver.findElement(By.xpath("//*[@id=\"login-button\"]"));
        Actions a= new Actions(driver);
        //1.Enter "standard_user" TEXT in Username field
        a.moveToElement(userNameTextBox).click().sendKeys("standard_user");

        //2. BUILD and PERFORM
        a.build().perform();

        //3.Enter an Incorrect password in Password field
        a.moveToElement(passwordTextBox).click().sendKeys("abcdefg");

        //4. BUILD and PERFORM
        a.build().perform();

        //5. Click LOGIN Button
        a.click(loginButton);

        //6. BUILD and PERFORM
        a.build().perform();

        String result="";
        try {
            WebElement expected = driver.findElement(By.xpath("//*[@id=\"page_wrapper\"]/footer/ul/li[2]/a"));
            result="OK";

        }catch(Exception e){
            result="ERROR";
        }
        assertEquals("ERROR",result);

    }

    @Test
    void testWhenUserIsIncorrectAndPasswordIsCorrect(){
        WebDriver driver= new ChromeDriver();
        driver.get("https://www.saucedemo.com/inventory.html"); //access the website
        driver.manage().window().maximize();

        WebElement userNameTextBox=driver.findElement(By.xpath("//*[@id=\"user-name\"]"));
        WebElement passwordTextBox=driver.findElement(By.xpath("//*[@id=\"password\"]"));
        WebElement loginButton=driver.findElement(By.xpath("//*[@id=\"login-button\"]"));
        Actions a= new Actions(driver);
        //1.Enter Incorrect Username in Username field
        a.moveToElement(userNameTextBox).click().sendKeys("Alina");

        //2. BUILD and PERFORM
        a.build().perform();

        //3.Enter "secret_sauce" TEXT in Password field
        a.moveToElement(passwordTextBox).click().sendKeys("secret_sauce");

        //4. BUILD and PERFORM
        a.build().perform();

        //5. Click LOGIN Button
        a.click(loginButton);

        //6. BUILD and PERFORM
        a.build().perform();

        String result="";
        try {
            WebElement expected = driver.findElement(By.xpath("//*[@id=\"page_wrapper\"]/footer/ul/li[2]/a"));
            result="OK";

        }catch(Exception e){
            result="ERROR";
        }
        assertEquals("ERROR",result);

    }

    @Test
    void testLoginWithProblem_UserUsername(){

        WebDriver driver= new ChromeDriver();
        driver.get("https://www.saucedemo.com/inventory.html"); //access the website
        driver.manage().window().maximize();

        WebElement userNameTextBox=driver.findElement(By.xpath("//*[@id=\"user-name\"]"));
        WebElement passwordTextBox=driver.findElement(By.xpath("//*[@id=\"password\"]"));
        WebElement loginButton=driver.findElement(By.xpath("//*[@id=\"login-button\"]"));


        Actions a= new Actions(driver);

        //1.Enter "problem_user" TEXT in Username field
        a.moveToElement(userNameTextBox).click().sendKeys("problem_user");

        //2.Build and Perform
        a.build().perform();

        //3.Enter "secret_sauce" TEXT in Password field
        a.moveToElement(passwordTextBox).click().sendKeys("secret_sauce");

        //4. BUILD and PERFORM
        a.build().perform();

        //5. Click LOGIN Button
        a.click(loginButton);

        //6. BUILD and PERFORM
        a.build().perform();

        String result="";
        try {
            WebElement expected = driver.findElement(By.cssSelector("/static/media/sauce-backpack-1200x1500.34e7aa42.jpg"));
            result="OK";
        }catch (Exception e){
            result="ERROR";
        }
        assertEquals("ERROR",result);
    }


    @Test
    void testIfTheLogoutButtonWorks(){
        WebDriver driver= new ChromeDriver();
        driver.get("https://www.saucedemo.com/inventory.html"); //access the website
        driver.manage().window().maximize();

        WebElement userNameTextBox=driver.findElement(By.xpath("//*[@id=\"user-name\"]"));
        WebElement passwordTextBox=driver.findElement(By.xpath("//*[@id=\"password\"]"));
        WebElement loginButton=driver.findElement(By.xpath("//*[@id=\"login-button\"]"));


        Actions a= new Actions(driver);

        //1.Enter "standard_user" TEXT in Username field
        a.moveToElement(userNameTextBox).click().sendKeys("standard_user");

        //2. BUILD and PERFORM
        a.build().perform();

        //3.Enter "secret_sauce" TEXT in Password field
        a.moveToElement(passwordTextBox).click().sendKeys("secret_sauce");

        //4. BUILD and PERFORM
        a.build().perform();

        //5. Click LOGIN Button
        a.moveToElement(loginButton).click();

        //6. BUILD and PERFORM
        a.build().perform();

        WebElement menu_Button=driver.findElement(By.xpath("//*[@id=\"react-burger-menu-btn\"]")); //identify the MENU_BUTTON

        //7.Click on Menu Button
        a.moveToElement(menu_Button).click();

        //8. Build and Perform
        a.build().perform();

        WebElement itemListButton1 =driver.findElement(By.xpath("//*[@id=\"menu_button_container\"]/div/div[2]")); //identify ItemListButton1

        //9.Move to element itemListButton1
        a.moveToElement(itemListButton1);

        WebElement itemListButton2 = driver.findElement(By.xpath("//*[@id=\"menu_button_container\"]/div/div[2]/div[1]/nav")); //identify ItemListButton2

        //10.Move to element itemListButton2
        a.moveToElement(itemListButton2);

        WebElement LogoutButton = driver.findElement(By.id("logout_sidebar_link")); //identify the LogoutButton

        //11.Click on LogutButton
        a.moveToElement(LogoutButton).click();

        //12.Build and Perform
        a.build().perform();

        String result="";
        try {
            WebElement expected = driver.findElement(By.xpath("//*[@id=\"login-button\"]"));
            result="OK";

        }catch(Exception e){
            result="ERROR";
        }
        assertEquals("OK",result);


    }
    @Test
    void testAboutButton(){
        WebDriver driver= new ChromeDriver();
        driver.get("https://www.saucedemo.com/inventory.html"); //access the website
        driver.manage().window().maximize();

        WebElement userNameTextBox=driver.findElement(By.xpath("//*[@id=\"user-name\"]"));
        WebElement passwordTextBox=driver.findElement(By.xpath("//*[@id=\"password\"]"));
        WebElement loginButton=driver.findElement(By.xpath("//*[@id=\"login-button\"]"));


        Actions a= new Actions(driver);

        //1.Enter "standard_user" TEXT in Username field
        a.moveToElement(userNameTextBox).click().sendKeys("standard_user");

        //2. BUILD and PERFORM
        a.build().perform();

        //3.Enter "secret_sauce" TEXT in Password field
        a.moveToElement(passwordTextBox).click().sendKeys("secret_sauce");

        //4. BUILD and PERFORM
        a.build().perform();

        //5. Click LOGIN Button
        a.moveToElement(loginButton).click();

        //6. BUILD and PERFORM
        a.build().perform();

        WebElement menu_Button=driver.findElement(By.xpath("//*[@id=\"react-burger-menu-btn\"]")); //identify the MENU_BUTTON

        //7.Click on Menu Button
        a.moveToElement(menu_Button).click();

        //8. Build and Perform
        a.build().perform();

        WebElement itemListButton1 =driver.findElement(By.xpath("//*[@id=\"menu_button_container\"]/div/div[2]")); //identify ItemListButton1

        //9.Move to element itemListButton1
        a.moveToElement(itemListButton1);

        WebElement itemListButton2 = driver.findElement(By.xpath("//*[@id=\"menu_button_container\"]/div/div[2]/div[1]/nav")); //identify ItemListButton2

        //10.Move to element itemListButton2
        a.moveToElement(itemListButton2);

        WebElement AboutButton = driver.findElement(By.id("about_sidebar_link")); //identify the AboutButton

        //11.Click on AboutButton
        a.moveToElement(AboutButton).click();

        //12.Build and Perform
        a.build().perform();

        String result="";
        try {
            WebElement expected = driver.findElement(By.xpath("//*[@id=\"headerMainNav\"]/div/nav/div"));
            result="OK";

        }catch(Exception e){
            result="ERROR";
        }
        assertEquals("OK",result);

    }

    @Test
    void testIfResetAppStateButtonIsDoingNothing(){
        WebDriver driver= new ChromeDriver();
        driver.get("https://www.saucedemo.com/inventory.html"); //access the website
        driver.manage().window().maximize();

        WebElement userNameTextBox=driver.findElement(By.xpath("//*[@id=\"user-name\"]"));
        WebElement passwordTextBox=driver.findElement(By.xpath("//*[@id=\"password\"]"));
        WebElement loginButton=driver.findElement(By.xpath("//*[@id=\"login-button\"]"));


        Actions a= new Actions(driver);

        //1.Enter "standard_user" TEXT in Username field
        a.moveToElement(userNameTextBox).click().sendKeys("standard_user");

        //2. BUILD and PERFORM
        a.build().perform();

        //3.Enter "secret_sauce" TEXT in Password field
        a.moveToElement(passwordTextBox).click().sendKeys("secret_sauce");

        //4. BUILD and PERFORM
        a.build().perform();

        //5. Click LOGIN Button
        a.moveToElement(loginButton).click();

        //6. BUILD and PERFORM
        a.build().perform();

        WebElement menu_Button=driver.findElement(By.xpath("//*[@id=\"react-burger-menu-btn\"]")); //identify the MENU_BUTTON

        //7.Click on Menu Button
        a.moveToElement(menu_Button).click();

        //8. Build and Perform
        a.build().perform();

        WebElement itemListButton1 =driver.findElement(By.xpath("//*[@id=\"menu_button_container\"]/div/div[2]")); //identify ItemListButton1

        //9.Move to element itemListButton1
        a.moveToElement(itemListButton1);

        WebElement itemListButton2 = driver.findElement(By.xpath("//*[@id=\"menu_button_container\"]/div/div[2]/div[1]/nav")); //identify ItemListButton2

        //10.Move to element itemListButton2
        a.moveToElement(itemListButton2);

        WebElement ResetAppStateButton = driver.findElement(By.id("reset_sidebar_link")); //identify the ResetAppStateButton

        //11.Click on ResetAppStateButton
        a.moveToElement(ResetAppStateButton).click();

        //12.Build and Perform
        a.build().perform();

        String result="";
        try {
            WebElement expected = driver.findElement(By.xpath("//*[@id=\"menu_button_container\"]/div/div[2]"));
            result="OK";

        }catch(Exception e){
            result="ERROR";
        }
        assertEquals("OK",result);

    }



    @Test
    void testAddToCartButtonForSauceLabsBackpackItem() {
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.saucedemo.com/inventory.html"); //access the website
        driver.manage().window().maximize();

        WebElement userNameTextBox = driver.findElement(By.xpath("//*[@id=\"user-name\"]"));
        WebElement passwordTextBox = driver.findElement(By.xpath("//*[@id=\"password\"]"));
        WebElement loginButton = driver.findElement(By.xpath("//*[@id=\"login-button\"]"));


        Actions a = new Actions(driver);

        //1.Enter "standard_user" TEXT in Username field
        a.moveToElement(userNameTextBox).click().sendKeys("standard_user");

        //2. BUILD and PERFORM
        a.build().perform();

        //3.Enter "secret_sauce" TEXT in Password field
        a.moveToElement(passwordTextBox).click().sendKeys("secret_sauce");

        //4. BUILD and PERFORM
        a.build().perform();

        //5. Click LOGIN Button
        a.click(loginButton);

        //6. BUILD and PERFORM
        a.build().perform();

        WebElement SauceLabsBackpackItem = driver.findElement(By.xpath("//*[@id=\"item_4_title_link\"]/div")); //identify the SauceLabsBackpackItem

        //7.Click on SauceLabsBackpackItem
        a.moveToElement(SauceLabsBackpackItem).click();

        //8.Build and Perform
        a.build().perform();

        WebElement SauceBackpackAddToCartButton = driver.findElement(By.xpath("//*[@id=\"add-to-cart-sauce-labs-backpack\"]")); //identify the BackpackAddToCartButton

        //9.Click on Add To Cart Button
        a.moveToElement(SauceBackpackAddToCartButton).click();

        //10.Build and Perform
        a.build().perform();

        String result = "";
        try {
            WebElement expected = driver.findElement(By.xpath("//*[@id=\"remove-sauce-labs-backpack\"]"));
            result = "OK";

        } catch (Exception e) {
            result = "ERROR";
        }
        assertEquals("OK", result);
    }

    @Test
    void testIfBackpackItemAppearInShoppingCartAfterPressingTheButtonAddToCart(){
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.saucedemo.com/inventory.html"); //access the website
        driver.manage().window().maximize();

        WebElement userNameTextBox = driver.findElement(By.xpath("//*[@id=\"user-name\"]"));
        WebElement passwordTextBox = driver.findElement(By.xpath("//*[@id=\"password\"]"));
        WebElement loginButton = driver.findElement(By.xpath("//*[@id=\"login-button\"]"));


        Actions a = new Actions(driver);

        //1.Enter "standard_user" TEXT in Username field
        a.moveToElement(userNameTextBox).click().sendKeys("standard_user");

        //2. BUILD and PERFORM
        a.build().perform();

        //3.Enter "secret_sauce" TEXT in Password field
        a.moveToElement(passwordTextBox).click().sendKeys("secret_sauce");

        //4. BUILD and PERFORM
        a.build().perform();

        //5. Click LOGIN Button
        a.click(loginButton);

        //6. BUILD and PERFORM
        a.build().perform();

        WebElement SauceLabsBackpackItem = driver.findElement(By.xpath("//*[@id=\"item_4_title_link\"]/div")); //identify the SauceLabsBackpackItem

        //7.Click on SauceLabsBackpackItem
        a.moveToElement(SauceLabsBackpackItem).click();

        //8.Build and Perform
        a.build().perform();

        WebElement SauceBackpackAddToCartButton = driver.findElement(By.xpath("//*[@id=\"add-to-cart-sauce-labs-backpack\"]")); //identify the BackpackAddToCartButton

        //9.Click on Add To Cart Button
        a.moveToElement(SauceBackpackAddToCartButton).click();

        //10.Build and Perform
        a.build().perform();

        WebElement ShoppingCart= driver.findElement(By.className("shopping_cart_link"));// identify the Shopping Cart Button

        //11.Enter Shopping Cart
        a.moveToElement(ShoppingCart).click();

        //12.Build and Perform
        a.build().perform();

        String result = "";
        try {
            WebElement expected = driver.findElement(By.xpath("//*[@id=\"item_4_title_link\"]/div"));
            result = "OK";

        } catch (Exception e) {
            result = "ERROR";
        }
        assertEquals("OK", result);


    }

    @Test
    void testIfWeCanPlaceAnOrder(){
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.saucedemo.com/inventory.html"); //access the website
        driver.manage().window().maximize();

        WebElement userNameTextBox = driver.findElement(By.xpath("//*[@id=\"user-name\"]"));
        WebElement passwordTextBox = driver.findElement(By.xpath("//*[@id=\"password\"]"));
        WebElement loginButton = driver.findElement(By.xpath("//*[@id=\"login-button\"]"));


        Actions a = new Actions(driver);

        //1.Enter "standard_user" TEXT in Username field
        a.moveToElement(userNameTextBox).click().sendKeys("standard_user");

        //2. BUILD and PERFORM
        a.build().perform();

        //3.Enter "secret_sauce" TEXT in Password field
        a.moveToElement(passwordTextBox).click().sendKeys("secret_sauce");

        //4. BUILD and PERFORM
        a.build().perform();

        //5. Click LOGIN Button
        a.click(loginButton);

        //6. BUILD and PERFORM
        a.build().perform();

        WebElement SauceLabsBackpackItem = driver.findElement(By.xpath("//*[@id=\"item_4_title_link\"]/div")); //identify the SauceLabsBackpackItem

        //7.Click on SauceLabsBackpackItem
        a.moveToElement(SauceLabsBackpackItem).click();

        //8.Build and Perform
        a.build().perform();

        WebElement SauceBackpackAddToCartButton = driver.findElement(By.xpath("//*[@id=\"add-to-cart-sauce-labs-backpack\"]")); //identify the BackpackAddToCartButton

        //9.Click on Add To Cart Button
        a.moveToElement(SauceBackpackAddToCartButton).click();

        //10.Build and Perform
        a.build().perform();

        WebElement ShoppingCart= driver.findElement(By.className("shopping_cart_link"));// identify the Shopping Cart Button

        //11.Enter Shopping Cart
        a.moveToElement(ShoppingCart).click();

        //12.Build and Perform
        a.build().perform();

        WebElement CheckoutButton = driver.findElement(By.xpath("//*[@id=\"checkout\"]"));

        //13.Click on CHECKOUT Button
        a.moveToElement(CheckoutButton).click();

        //14.Build and Perform
        a.build().perform();

        WebElement FirstNameField= driver.findElement(By.xpath("//*[@id=\"first-name\"]")); //identify the First Name Field

        //15.Send Alina TEXT to First Name Field
        a.moveToElement(FirstNameField).click().sendKeys("Alina");

        //16.Build and Perform
        a.build().perform();

        WebElement LastNameField = driver.findElement(By.xpath("//*[@id=\"last-name\"]")); //identify the Last Name Field

        //17.Send Brisan TEXT to Last Name Field
        a.moveToElement(LastNameField).click().sendKeys("Brisan");

        //18.Build and Perform
        a.build().perform();

        WebElement ZipCodeField = driver.findElement(By.xpath("//*[@id=\"postal-code\"]")); //identify the ZipCode Field

        //19.Send "400672" Zip Code to ZipCode Field
        a.moveToElement(ZipCodeField).click().sendKeys("400672");

        //20.Build and Perform
        a.build().perform();

        WebElement ContinueButton = driver.findElement(By.xpath("//*[@id=\"continue\"]")); //identify the ContinueButton

        //21.Click on Continue Button
        a.moveToElement(ContinueButton).click();

        //22.Build and Perform
        a.build().perform();

        WebElement FinishButton = driver.findElement(By.xpath("//*[@id=\"finish\"]")); //identify the FinishButton

        //23.Click on Finish Button
        a.moveToElement(FinishButton).click();

        //24.Build and Perform
        a.build().perform();

        String result="";
        try{
            WebElement expected = driver.findElement(By.xpath("//*[@id=\"checkout_complete_container\"]/h2"));
            result="OK";
        }catch (Exception e){
            result="ERROR";
        }
        assertEquals("OK",result);
    }

    @Test
    void testIfProductsRemainAddedToTheCartAfterLoggingOut(){
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.saucedemo.com/inventory.html"); //access the website
        driver.manage().window().maximize();

        WebElement userNameTextBox = driver.findElement(By.xpath("//*[@id=\"user-name\"]"));
        WebElement passwordTextBox = driver.findElement(By.xpath("//*[@id=\"password\"]"));
        WebElement loginButton = driver.findElement(By.xpath("//*[@id=\"login-button\"]"));


        Actions a = new Actions(driver);

        //1.Enter "standard_user" TEXT in Username field
        a.moveToElement(userNameTextBox).click().sendKeys("standard_user");

        //2. BUILD and PERFORM
        a.build().perform();

        //3.Enter "secret_sauce" TEXT in Password field
        a.moveToElement(passwordTextBox).click().sendKeys("secret_sauce");

        //4. BUILD and PERFORM
        a.build().perform();

        //5. Click LOGIN Button
        a.click(loginButton);

        //6. BUILD and PERFORM
        a.build().perform();

        WebElement SauceLabsBackpackItem = driver.findElement(By.xpath("//*[@id=\"item_4_title_link\"]/div")); //identify the SauceLabsBackpackItem

        //7.Click on SauceLabsBackpackItem
        a.moveToElement(SauceLabsBackpackItem).click();

        //8.Build and Perform
        a.build().perform();

        WebElement SauceBackpackAddToCartButton = driver.findElement(By.xpath("//*[@id=\"add-to-cart-sauce-labs-backpack\"]")); //identify the BackpackAddToCartButton

        //9.Click on Add To Cart Button
        a.moveToElement(SauceBackpackAddToCartButton).click();

        //10.Build and Perform
        a.build().perform();

        WebElement BackToProductsButton=driver.findElement(By.xpath("//*[@id=\"back-to-products\"]"));

        //11.Click on Back to Products Button
        a.moveToElement(BackToProductsButton).click();

        //12.Build and Perform
        a.build().perform();

        WebElement BikeLight=driver.findElement(By.xpath("//*[@id=\"item_0_title_link\"]/div"));

        //13.Click on BikeLight Item
        a.moveToElement(BikeLight).click();

        //14.Build and Perform
        a.build().perform();

        WebElement BikeLightAddToCartButton=driver.findElement(By.xpath("//*[@id=\"add-to-cart-sauce-labs-bike-light\"]"));

        //15.Click on BikeLight AddToCart Button
        a.moveToElement(BikeLightAddToCartButton).click();

        //16.Build and Perform
        a.build().perform();

        WebElement BackToProductsButton2=driver.findElement(By.xpath("//*[@id=\"back-to-products\"]"));

        //17.Click on Back to Products Button
        a.moveToElement(BackToProductsButton2).click();

        //18.Build and Perform
        a.build().perform();

        WebElement menu_Button=driver.findElement(By.xpath("//*[@id=\"react-burger-menu-btn\"]")); //identify the MENU_BUTTON

        //19.Click on Menu Button
        a.moveToElement(menu_Button).click();

        //20. Build and Perform
        a.build().perform();

        WebElement itemListButton1 =driver.findElement(By.xpath("//*[@id=\"menu_button_container\"]/div/div[2]")); //identify ItemListButton1

        //21.Move to element itemListButton1
        a.moveToElement(itemListButton1);

        WebElement itemListButton2 = driver.findElement(By.xpath("//*[@id=\"menu_button_container\"]/div/div[2]/div[1]/nav")); //identify ItemListButton2

        //22.Move to element itemListButton2
        a.moveToElement(itemListButton2);

        WebElement LogoutButton = driver.findElement(By.id("logout_sidebar_link")); //identify the LogoutButton

        //23.Click on LogutButton
        a.moveToElement(LogoutButton).click();

        //24.Build and Perform
        a.build().perform();

        WebElement userNameTextBox2=driver.findElement(By.xpath("//*[@id=\"user-name\"]"));

        //25.Enter "standard_user" TEXT in Username field
        a.moveToElement(userNameTextBox2).click().sendKeys("standard_user");

        //26. BUILD and PERFORM
        a.build().perform();

        WebElement passwordTextBox2=driver.findElement(By.xpath("//*[@id=\"password\"]"));

        //27.Enter "secret_sauce" TEXT in Password field
        a.moveToElement(passwordTextBox2).click().sendKeys("secret_sauce");

        //28. BUILD and PERFORM
        a.build().perform();

        WebElement loginButton2=driver.findElement(By.xpath("//*[@id=\"login-button\"]"));

        //29. Click LOGIN Button
        a.click(loginButton2);

        //Build and Perform
        a.build().perform();

        String result = "";
        try {
            WebElement expected = driver.findElement(By.xpath("//*[@id=\"remove-sauce-labs-backpack\"]"));
            WebElement expected2 = driver.findElement(By.xpath("//*[@id=\"remove-sauce-labs-bike-light\"]"));
            result = "OK";

        } catch (Exception e) {
            result = "ERROR";
        }
        assertEquals("OK", result);

    }

    @Test
    void testIfSocialFacebookButtonRedirectUsToTheirFacebookPage  (){
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.saucedemo.com/inventory.html"); //access the website
        driver.manage().window().maximize();

        WebElement userNameTextBox = driver.findElement(By.xpath("//*[@id=\"user-name\"]"));
        WebElement passwordTextBox = driver.findElement(By.xpath("//*[@id=\"password\"]"));
        WebElement loginButton = driver.findElement(By.xpath("//*[@id=\"login-button\"]"));


        Actions a = new Actions(driver);

        //1.Enter "standard_user" TEXT in Username field
        a.moveToElement(userNameTextBox).click().sendKeys("standard_user");

        //2. BUILD and PERFORM
        a.build().perform();

        //3.Enter "secret_sauce" TEXT in Password field
        a.moveToElement(passwordTextBox).click().sendKeys("secret_sauce");

        //4. BUILD and PERFORM
        a.build().perform();

        //5. Click LOGIN Button
        a.click(loginButton);

        //6. BUILD and PERFORM
        a.build().perform();

        WebElement SocialFacebookButton = driver.findElement(By.xpath("//*[@id=\"page_wrapper\"]/footer/ul/li[2]/a"));

        //7.Click on Facebook Button
        a.moveToElement(SocialFacebookButton).click();

        //8.Build and Perform
        a.build().perform();


        String result = "";
        try {
            String winHandleBefore = driver.getWindowHandle();
            for(String winHandle : driver.getWindowHandles()){
                driver.switchTo().window(winHandle);
            }
            String  URL = driver.getCurrentUrl();
            result = URL;

        } catch (Exception e) {
            result = "ERROR";
        }
        assertEquals("https://www.facebook.com/saucelabs", result);
    }


    //Stress testing
    @Test
    void stressTestingAddToCart (){
        WebDriver driver= new ChromeDriver();
        loginFunction(driver);
        for (int i=0; i<100;i++ ){
            addToCartFunction(driver);
        }
    }
    void loginFunction(WebDriver driver){

        driver.get("https://www.saucedemo.com/inventory.html"); //access the website
        driver.manage().window().maximize();

        WebElement userNameTextBox=driver.findElement(By.xpath("//*[@id=\"user-name\"]"));
        WebElement passwordTextBox=driver.findElement(By.xpath("//*[@id=\"password\"]"));
        WebElement loginButton=driver.findElement(By.xpath("//*[@id=\"login-button\"]"));
        Actions a= new Actions(driver);
        //1.Enter "standard_user" TEXT in Username field
        a.moveToElement(userNameTextBox).click().sendKeys("standard_user");

        //2. BUILD and PERFORM
        a.build().perform();

        //3.Enter password in Password field
        a.moveToElement(passwordTextBox).click().sendKeys("secret_sauce");

        //4. BUILD and PERFORM
        a.build().perform();

        //5. Click LOGIN Button
        a.click(loginButton);

        //6. BUILD and PERFORM
        a.build().perform();
    }
    void addToCartFunction(WebDriver driver){
        Actions a= new Actions(driver);

        WebElement SauceBackpackAddToCartButton = driver.findElement(By.xpath("//*[@id=\"add-to-cart-sauce-labs-backpack\"]")); //identify the BackpackAddToCartButton

        //Click on Add To Cart Button
        a.moveToElement(SauceBackpackAddToCartButton).click();

        //Build and Perform
        a.build().perform();


        WebElement RemoveButton = driver.findElement(By.xpath("//*[@id=\"remove-sauce-labs-backpack\"]")); //identify remove button

        //Click on Remove button
        a.moveToElement(RemoveButton).click();

        //Build and Perform
        a.build().perform();

    }
}
